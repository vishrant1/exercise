package com.brevitaz.exercise.oom.cafe;

public class Tea extends Beverage {
    @Override
    public String toString() {
        return "Tea";
    }
}
