package com.brevitaz.exercise.oom.cafe;

public class CafeMain {
    public static void main(String[] args) {
        Servant servant = new Servant();
        Beverage btea = new Tea();
        Beverage bcoffee = new Coffee();
        servant.name = "Prakash";
        servant.serve(btea);
    }
}
