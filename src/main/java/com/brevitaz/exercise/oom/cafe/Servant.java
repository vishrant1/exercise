package com.brevitaz.exercise.oom.cafe;

public class Servant extends Person {
    public void serve(Beverage b) {
        System.out.println(name + " is serving " + b);
    }
}
