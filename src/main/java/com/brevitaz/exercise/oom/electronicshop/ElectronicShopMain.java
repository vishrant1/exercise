package com.brevitaz.exercise.oom.electronicshop;

public class ElectronicShopMain {
    public static void main(String[] args) {
        Seller seller = new Seller();
        seller.setName("Bill");
        Buyer buyer = new Buyer();
        buyer.setName("Ram");
        Product product = new Product();
        product.productName = "Laptop";
        seller.selling(product,buyer);
    }

}
