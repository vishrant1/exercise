package com.brevitaz.exercise.oom.electronicshop;

public class Seller extends Person {
    public void selling(Product product, Buyer buyer) {
        System.out.println(getName() + " sells " + product.productName + " to " + buyer.getName() + " at low cost.");
    }
}
