package com.brevitaz.exercise.oom.drivingschool;

public class Bike extends Vehicle {
    public void start() {
        System.out.println(name + " is Starting");
    }

    public void stop() {    
        System.out.println(name + " is Stop");
    }
}
