package com.brevitaz.exercise.oom.drivingschool;

public class DrivingSchoolMain {
    public static void main(String[] args) {
        Driver driver = new Driver();
        Bike bike = new Bike();
        driver.setName("raj");
        bike.name = "bike";
        bike.model = "Splender";
        driver.drive(bike);
        System.out.println(driver.getName() + " is driving " + bike.model + " " + bike.name);
    }
}
