package com.brevitaz.exercise.oom.drivingschool;

public class Driver extends Person {
    public void drive(Bike b) {
        b.start();
        b.stop();
        //System.out.println("driving vehical");
    }
}
