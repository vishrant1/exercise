package com.brevitaz.exercise.oom.school;

public class Teacher extends Person {
    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    private String teacherName;
    private String subject;

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void teach(Student student) {
        System.out.print(teacherName + " is teaching " + subject + " to " + student.studentName);
    }
}
