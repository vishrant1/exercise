package com.brevitaz.exercise.oom.school;

public class SchoolMain {
    public static void main(String[] args) {
        Teacher teacher = new Teacher();
        teacher.setTeacherName("Kapil");
        teacher.setSubject("java");
        Student student = new Student();
        student.setSname("Sunil");
        teacher.teach(student);
    }
}
