package com.brevitaz.exercise.oom.travelagency;

public class Person {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public void travel(Location location, Car car, Road road) {
        System.out.println(this.name + " is travelling to " + location.locationName + " in " + car.model +
                " with number " + car.rgNo + " via " + road.roadName);
    }
}
