package com.brevitaz.exercise.oom.travelagency;

public class TravelAgencyMain {
    public static void main(String[] args) {
        Person person = new Person();
        person.setName("Tarang");
        Traveller traveller = new Traveller();
        traveller.name = "tarang";
        Road road = new Road();
        road.roadName = "Express Highway";
        Location location = new Location();
        location.locationName = "Baroda";
        Car car = new Car();
        car.model = "Maruti Swift";
        car.rgNo = "GJ1 KG 5620";
        person.travel(location, car, road);
    }
}
