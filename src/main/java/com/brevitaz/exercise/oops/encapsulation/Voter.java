package com.brevitaz.exercise.oops.encapsulation;

public class Voter extends Person {
    public static void main(String[] args) {
        Person person = new Person();
        person.setName("Ramesh");
        System.out.print(person.getName()+" is ");
        person.setAge(19);
    }
}
