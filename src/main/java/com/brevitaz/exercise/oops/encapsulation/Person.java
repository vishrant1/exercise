package com.brevitaz.exercise.oops.encapsulation;

public class Person {
    private String name;
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age > 18) {
            this.age = age;
            System.out.print("able for voting.");
        } else {
            System.out.print("not able for voting.");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
