package com.brevitaz.exercise.oops.abstraction;

public class Main {
    public static void main(String[] args) {
        Bike honda = new Honda();
//        Bike hero = new Hero();
        honda.run();
        honda.start();

    }
}
