package com.brevitaz.exercise.oops.abstraction;

abstract class Bike {
    Bike() {
        System.out.println("Bike class created.");
    }

    abstract void run();
    void start() {
        System.out.println("Bike start");
    }
}
