package com.brevitaz.exercise.oops.abstraction;

public class Honda extends Bike {
    @Override
    void run() {
        System.out.println("Honda class.");
    }
}
