package com.brevitaz.exercise.oops.abstraction;

public class Hero extends Bike {
    void run() {
        System.out.println("Hero class.");
    }
}
