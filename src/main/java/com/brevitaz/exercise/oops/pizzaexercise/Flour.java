package com.brevitaz.exercise.oops.pizzaexercise;

public class Flour extends DoughIngredient {
    @Override
    public String toString() {
        return "Flour";
    }
}
