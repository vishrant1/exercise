package com.brevitaz.exercise.oops.pizzaexercise;

public interface Bakeable {
    BakedFood bake();
}
