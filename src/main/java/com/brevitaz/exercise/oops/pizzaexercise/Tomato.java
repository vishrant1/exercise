package com.brevitaz.exercise.oops.pizzaexercise;

public class Tomato extends ToppingIngredient {
    @Override
    public String toString() {
        return "Tomato";
    }
}
