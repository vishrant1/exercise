package com.brevitaz.exercise.oops.pizzaexercise;

import java.util.Arrays;

public class PizzaStyle {
    private String baseType;

    public String getBaseType() {
        return baseType;
    }

    public void setBaseType(String baseType) {
        this.baseType = baseType;
    }

    private String[] topping;
    private String[] sauce;
    private String[] cheese;

    @Override
    public String toString() {
        return "PizzaStyle{" +
                "topping=" + Arrays.toString(topping) +
                ", sauce=" + Arrays.toString(sauce) +
                ", cheese=" + Arrays.toString(cheese) +
                '}';
    }

    public String[] getCheese() {
        return cheese;
    }

    public void setCheese(String[] cheese) {
        this.cheese = cheese;
    }

    public String[] getTopping() {
        return topping;
    }

    public void setTopping(String[] topping) {
        this.topping = topping;
    }

    public String[] getSauce() {
        return sauce;
    }

    public void setSauce(String[] sauce) {
        this.sauce = sauce;
    }

}
