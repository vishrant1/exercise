package com.brevitaz.exercise.oops.pizzaexercise;

public class RedPepperika extends ToppingIngredient {
    @Override
    public String toString() {
        return "RedPepperika";
    }
}
