package com.brevitaz.exercise.oops.pizzaexercise;

public class Servant {
    public void serve(Pizza pizza) {
        System.out.println("Servant serve the Pizza");
    }

}
