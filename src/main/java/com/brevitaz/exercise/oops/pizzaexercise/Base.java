package com.brevitaz.exercise.oops.pizzaexercise;

import java.lang.String;
import java.util.ArrayList;
import java.util.List;

public class Base implements Bakeable {

    private String type;
    private Dough dough;
    private List<Sauce> sauces = new ArrayList<>();
    private List<ToppingIngredient> toppings = new ArrayList<>();
    private List<Cheese> cheeses = new ArrayList<>();

    public void setBaseType(String baseType) {
        this.type = baseType;
    }

    public Base(Dough dough) {
        this.dough = dough;
    }

    public void spreadSause(String[] sauces) {
        for (String sauce : sauces) {
            if (sauce.equalsIgnoreCase("tomato")) {
                Sauce tomato = new TomatoSauce();
                this.sauces.add(tomato);
            } else if (sauce.equalsIgnoreCase("bbq")) {
                Sauce bbq = new BBQSauce();
                this.sauces.add(bbq);
            } else if (sauce.equalsIgnoreCase("pesto")) {
                Sauce pesto = new PestoSauce();
                this.sauces.add(pesto);
            } else if (sauce.equalsIgnoreCase("sweetonionsauce")) {
                Sauce sweetOnionSauce = new SweetOnionSauce();
                this.sauces.add(sweetOnionSauce);
            } else if (sauce.equalsIgnoreCase("AfricanPiriPirisauce")) {
                Sauce africanPiriPiriSauce = new AfricanPiriPiriSauce();
                this.sauces.add(africanPiriPiriSauce);
            } else if (sauce.equalsIgnoreCase("garlicbuttersauce")) {
                Sauce garlicButterSauce = new GarlicButterSauce();
                this.sauces.add(garlicButterSauce);
            } else {
                System.out.println("Sorry, this " + sauce + " sauce is not avilable.");
            }
        }
        System.out.println(this.sauces);
        System.out.println("Spread the above sauces on the base.");
    }

    public void addTopping(String[] toppings) {
        for (String topping : toppings) {
            if (topping.equalsIgnoreCase("corn")) {
                ToppingIngredient corn = new Corn();
                this.toppings.add(corn);
            } else if (topping.equalsIgnoreCase("olive")) {
                ToppingIngredient olive = new Olive();
                this.toppings.add(olive);
            } else if (topping.equalsIgnoreCase("tomato")) {
                ToppingIngredient tomato = new Tomato();
                this.toppings.add(tomato);
            } else if (topping.equalsIgnoreCase("paneer")) {
                ToppingIngredient paneer = new Paneer();
                this.toppings.add(paneer);
            } else if (topping.equalsIgnoreCase("mushroom")) {
                ToppingIngredient mushroom = new Mushroom();
                this.toppings.add(mushroom);
            } else if (topping.equalsIgnoreCase("redpepperika")) {
                ToppingIngredient redPepperika = new RedPepperika();
                this.toppings.add(redPepperika);
            } else if (topping.equalsIgnoreCase("jalapeno")) {
                ToppingIngredient jalapeno = new Jalapeno();
                this.toppings.add(jalapeno);
            } else if (topping.equalsIgnoreCase("redCapsicum")) {
                ToppingIngredient redCapsicum = new RedCapsicum();
                this.toppings.add(redCapsicum);
            } else if (topping.equalsIgnoreCase("greenCapsicum")) {
                ToppingIngredient greenCapsicum = new GreenCapsicum();
                this.toppings.add(greenCapsicum);
            } else {
                System.out.println("Sorry, " + topping + " is not avilable in the toppings.");
            }
        }
        System.out.println(this.toppings);
        System.out.println("Add the above toppings on the base");
    }

    public void spreadCheese(String[] cheeses) {
        for (String cheese : cheeses) {
            if (cheese.equalsIgnoreCase("mozzarella")) {
                Cheese mozzarella = new Mozzarella();
                this.cheeses.add(mozzarella);
            } else if (cheese.equalsIgnoreCase("ricotta")) {
                Cheese ricotta = new Ricotta();
                this.cheeses.add(ricotta);
            }else if (cheese.equalsIgnoreCase("creamwhitecheese")) {
                Cheese creamWhiteCheese = new CreamWhiteCheese();
                this.cheeses.add(creamWhiteCheese);
            }else if (cheese.equalsIgnoreCase("montereyjack")) {
                Cheese montereyJack = new MontereyJack();
                this.cheeses.add(montereyJack);
            }else if (cheese.equalsIgnoreCase("creamorangecheese")) {
                Cheese creamOrangeCheese = new CreamOrangeCheese();
                this.cheeses.add(creamOrangeCheese);
            }else if (cheese.equalsIgnoreCase("colby")) {
                Cheese colby = new Colby();
                this.cheeses.add(colby);
            }else if (cheese.equalsIgnoreCase("orangecheddar")) {
                Cheese orangeCheddar = new OrangeCheddar();
                this.cheeses.add(orangeCheddar);
            } else if (cheese.equalsIgnoreCase("cheddar")) {
                Cheese cheddar = new Cheddar();
                this.cheeses.add(cheddar);
            } else {
                System.out.println("Sorry, this " + cheese + " cheese is not avilable.");
            }
        }
        System.out.println("Spread the " + this.cheeses + "cheese.");
    }

    @Override
    public BakedFood bake() {
        Pizza pizza = new Pizza(this);
        System.out.println("Baking the pizza");
        return pizza;
    }

    @Override
    public String toString() {
        return "Base{" +
                "\n\t type='" + type + '\'' +
                "\n\t dough=" + dough +
                "\n\t sauces=" + sauces +
                "\n\t toppings=" + toppings +
                "\n\t cheeses=" + cheeses +
                "\n\t }";
    }
}
