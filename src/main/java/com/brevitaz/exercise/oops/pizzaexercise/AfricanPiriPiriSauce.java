package com.brevitaz.exercise.oops.pizzaexercise;

public class AfricanPiriPiriSauce extends Sauce {
    @Override
    public String toString() {
        return "African Piri-Piri Sauce";
    }
}
