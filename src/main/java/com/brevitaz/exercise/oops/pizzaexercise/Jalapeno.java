package com.brevitaz.exercise.oops.pizzaexercise;

public class Jalapeno extends ToppingIngredient {
    @Override
    public String toString() {
        return "Jalapeno";
    }
}
