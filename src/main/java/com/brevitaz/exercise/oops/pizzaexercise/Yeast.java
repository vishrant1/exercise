package com.brevitaz.exercise.oops.pizzaexercise;

public class Yeast extends DoughIngredient {
    @Override
    public String toString() {
        return "Yeast";
    }
}
