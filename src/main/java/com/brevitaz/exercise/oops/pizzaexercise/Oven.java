package com.brevitaz.exercise.oops.pizzaexercise;

public class Oven {
    private boolean isOn = false;

    @Override
    public String toString() {
        return "Oven{" +
                "isOn=" + isOn +
                '}';
    }

    public BakedFood bake(Bakeable bakeable) {
        this.isOn = true;
        return bakeable.bake();
    }
}
