package com.brevitaz.exercise.oops.pizzaexercise;

public class CreamOrangeCheese extends Cheese {
    @Override
    public String toString() {
        return "CreamOrangeCheese";
    }
}
