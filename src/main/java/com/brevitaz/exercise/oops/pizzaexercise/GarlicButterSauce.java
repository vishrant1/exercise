package com.brevitaz.exercise.oops.pizzaexercise;

public class GarlicButterSauce extends Sauce {
    @Override
    public String toString() {
        return "GarlicButterSauce";
    }
}
