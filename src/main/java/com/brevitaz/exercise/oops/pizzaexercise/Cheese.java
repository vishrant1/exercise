package com.brevitaz.exercise.oops.pizzaexercise;

public class Cheese extends ToppingIngredient {
    @Override
    public String toString() {
        return "Cheese";
    }

}
