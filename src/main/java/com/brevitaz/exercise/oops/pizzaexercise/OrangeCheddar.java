package com.brevitaz.exercise.oops.pizzaexercise;

public class OrangeCheddar extends Cheese {
    @Override
    public String toString() {
        return "OrangeCheddar";
    }
}
