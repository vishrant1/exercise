package com.brevitaz.exercise.oops.pizzaexercise;

public class Pizza extends BakedFood {
    private Base base;


    @Override
    public String toString() {
        return "Pizza{" +
                "\n base=" + base +
                "\n }";
    }

    public Pizza (Base base) {
        this.base = base;
     }
}
