package com.brevitaz.exercise.oops.pizzaexercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Dough {
    private List<DoughIngredient> doughIngredient = new ArrayList<>();

    public Dough(DoughIngredient[] doughIngredients) {
        doughIngredient.addAll(Arrays.asList(doughIngredients));
    }

    @Override
    public String toString() {
        return "Dough{" +
                "doughIngredient=" + doughIngredient +
                '}';
    }
}