package com.brevitaz.exercise.oops.pizzaexercise;

public class Chef {

    private Dough makeDough(DoughIngredient[] doughIngredients) {
        for (DoughIngredient doughIngredient : doughIngredients) {
            System.out.println(doughIngredient);
        }
        System.out.println("Mix above ingredients and make the dough");
        Dough dough = new Dough(doughIngredients);
        System.out.println("Now dough is ready.");
        return dough;
    }

    private Base rollDough(Dough dough) {
        System.out.println("Roll the dough and make the base");
        Base base = new Base(dough);
        System.out.println("Now the base is ready.");
        return base;
    }

    private void cut(Pizza pizza) {
        System.out.println("Cut the pizza");
    }

    public Pizza makePizza(PizzaStyle pizzaStyle) {
        DoughIngredient flour = new Flour();
        DoughIngredient salt = new Salt();
        DoughIngredient oil = new Oil();
        DoughIngredient yeast = new Yeast();
        DoughIngredient water = new Water();
        DoughIngredient[] doughIngredients = new DoughIngredient[]{flour, salt, oil, yeast, water};
        Dough readyDough = makeDough(doughIngredients);
        Base base = rollDough(readyDough);
        base.setBaseType(pizzaStyle.getBaseType());
        base.spreadSause(pizzaStyle.getSauce());
        base.addTopping(pizzaStyle.getTopping());
        base.spreadCheese(pizzaStyle.getCheese());
        Oven oven = new Oven();
        Pizza pizza = (Pizza) oven.bake(base);
        System.out.println(pizza);
        cut(pizza);
        return pizza;
    }
}