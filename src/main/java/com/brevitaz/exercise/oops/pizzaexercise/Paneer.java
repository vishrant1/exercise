package com.brevitaz.exercise.oops.pizzaexercise;

public class Paneer extends ToppingIngredient {
    @Override
    public String toString() {
        return "Paneer";
    }
}
