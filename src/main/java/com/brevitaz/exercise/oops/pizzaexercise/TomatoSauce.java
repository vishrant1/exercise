package com.brevitaz.exercise.oops.pizzaexercise;

public class TomatoSauce extends Sauce {
    @Override
    public String toString() {
        return "TomatoSauce";
    }
}
