package com.brevitaz.exercise.oops.pizzaexercise;

public class Olive extends ToppingIngredient {
    @Override
    public String toString() {
        return "Olive";
    }
}
