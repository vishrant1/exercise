package com.brevitaz.exercise.oops.pizzaexercise;

public class Oil extends DoughIngredient {
    @Override
    public String toString() {
        return "Oil";
    }
}
