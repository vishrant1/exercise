package com.brevitaz.exercise.oops.pizzaexercise;

public class PizzaMain {
    public static void main(String[] args) {
        Chef chef = new Chef();
        PizzaStyle pizzaStyle = new PizzaStyle();
        pizzaStyle.setBaseType("Thin crust");
        pizzaStyle.setTopping(new String[] {"mushroom", "redpepperika"});
        pizzaStyle.setSauce(new String[] {"AfricanPiriPiriSauce"});
        pizzaStyle.setCheese(new String[] {"Mozzarella", "Mozzarella"});
        Pizza pizza = chef.makePizza(pizzaStyle);
        Servant servant = new Servant();
        servant.serve(pizza);
    }
}