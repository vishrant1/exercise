package com.brevitaz.exercise.oops.pizzaexercise;

public class Mushroom extends ToppingIngredient {
    @Override
    public String toString() {
        return "Mushroom";
    }
}
