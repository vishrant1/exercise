package com.brevitaz.exercise.oops.pizzaexercise;

public class CreamWhiteCheese extends Cheese {
    @Override
    public String toString() {
        return "CreamWhiteCheese";
    }
}
