package com.brevitaz.exercise.oops.pizzaexercise;

public class Salt extends DoughIngredient {
    @Override
    public String toString() {
        return "Salt";
    }
}
