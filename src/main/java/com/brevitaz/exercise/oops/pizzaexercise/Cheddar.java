package com.brevitaz.exercise.oops.pizzaexercise;

public class Cheddar extends Cheese {
    @Override
    public String toString() {
        return "Cheddar";
    }
}
