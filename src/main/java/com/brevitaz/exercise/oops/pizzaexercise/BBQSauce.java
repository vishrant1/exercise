package com.brevitaz.exercise.oops.pizzaexercise;

public class BBQSauce extends Sauce {
    @Override
    public String toString() {
        return "BBQSauce";
    }
}
