package com.brevitaz.exercise.oops.pizzaexercise;

public class Water extends DoughIngredient {
    @Override
    public String toString() {
        return "Water";
    }
}
