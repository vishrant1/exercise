package com.brevitaz.exercise.oops.pizzaexercise;

public class Ricotta extends Cheese {
    @Override
    public String toString() {
        return "Ricotta";
    }
}
