package com.brevitaz.exercise.oops.pizzaexercise;

public class Corn extends ToppingIngredient {
    @Override
    public String toString() {
        return "Corn";
    }
}
