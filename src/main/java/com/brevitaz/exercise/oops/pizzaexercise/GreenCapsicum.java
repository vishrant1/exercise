package com.brevitaz.exercise.oops.pizzaexercise;

public class GreenCapsicum extends ToppingIngredient {
    @Override
    public String toString() {
        return "GreenCapsicum";
    }
}
