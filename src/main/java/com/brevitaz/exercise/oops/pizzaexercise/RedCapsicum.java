package com.brevitaz.exercise.oops.pizzaexercise;

public class RedCapsicum extends ToppingIngredient {
    @Override
    public String toString() {
        return "RedCapsicum";
    }
}
