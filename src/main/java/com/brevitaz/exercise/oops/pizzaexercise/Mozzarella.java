package com.brevitaz.exercise.oops.pizzaexercise;

public class Mozzarella extends Cheese {
    @Override
    public String toString() {
        return "Mozzarella";
    }
}
