package com.brevitaz.exercise.oops.pizzaexercise;

public class SweetOnionSauce extends Sauce {
    @Override
    public String toString() {
        return "SweetOnionSauce";
    }
}
