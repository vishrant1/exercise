package com.brevitaz.exercise.logical;

/**
 * Problem 2. Write a program in Java to make such a pattern like a pyramid with a number that will repeat the number in the
 * same row. Go to the editor
 * <p>
 * 1
 * 2 2
 * 3 3 3
 * 4 4 4 4
 */
public class Pattern {
    public static void main(String[] args) {
        int numberOfRow = 4;
        printPattern(numberOfRow);
    }

    public static void printPattern(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = n - i; j >= 1; j--) {
                System.out.print(" ");
            }
            for (int k = 1; k <= i; k++) {
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }
}
