package com.brevitaz.exercise.logical;

import java.util.Scanner;

/**
 * Problem 1.  Write a method that returns 'Fizz' for multiples of three and 'Buzz’ for the multiples of five. For
 * numbers which are multiples of both three and five return 'FizzBuzz'.For numbers that are neither, return the input
 * number.
 */
public class FizzBuzz {
    public static void main(String[] args) {
        System.out.println("Enter number either 3 or 5 : ");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        //System.out.println(number);
        FizzBuzz fb = new FizzBuzz();
        String ans = fb.checkStatus(number);
        System.out.println(ans);
    }

    public String checkStatus(int no) {
        return (no % 3 == 0 && no % 5 == 0 ? "FizzBuzz" : no % 3 == 0 ? "Fizz" : no % 5 == 0 ?
                "Buzz" : Integer.toString(no));
    }
}
