package com.brevitaz.exercise.logical;

import java.util.Scanner;

/**
 * Problem 3. Write programm to find all divisors of given no.
 * <p>
 * eg 1. No : 50
 * expected output : 1,2,5,10,25
 * <p>
 * eg 2. No : 16
 * expected output : 1,2,4,8
 */
public class Divisor {
    public static void main(String[] args) {
        System.out.println("Enter a number to find divisor : ");
        Scanner sc = new Scanner(System.in);
        int no = sc.nextInt();
        System.out.println("Divisors are : ");
        findDivisor(no);
    }

    public static void findDivisor(int no) {
        for (int i = 1; i <= no / 2; i++) {
            if (no % i == 0) {
                System.out.print(i + "  ");
            }
        }
    }
}
